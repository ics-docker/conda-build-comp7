# conda-build-comp7

[Docker](https://www.docker.com) image inspired by conda-forge
[linux-anvil-comp7](https://github.com/conda-forge/docker-images/tree/master/linux-anvil-comp7) image.

The image is based on centos:6 to ensure a large compatibily of the binaries produced.
Binaries are supposed to be built with anaconda compilers.

This image is for testing purpose. Should replace the conda-build image in the end.


## Building

This image is built automatically by gitlab-ci.

## How to use this image

To create a conda package, you must write a conda build recipe: https://conda.io/docs/user-guide/tutorials/build-pkgs.html
